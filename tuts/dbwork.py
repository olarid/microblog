import mysql.connector

class dbwork:

    def __init__(self):
        print('Got to db init')

        db = mysql.connector.connect(
            host="localhost",
            user="root",
            password=""
        )
        mycursor = db.cursor()

        #Create Database
        mycursor.execute("CREATE DATABASE IF NOT EXISTS dbpython;")

        #Create Table
        self.db = mysql.connector.connect(
            host="localhost",
            user="root",
            password="",
            database="dbpython"
        )


        mycursor = self.db.cursor()

        mycursor.execute(
            """CREATE TABLE IF NOT EXISTS tbstudents (id int auto_increment primary key, student_name varchar(200), 
                matric varchar(20), level varchar(20), date_created datetime default current_timestamp);"""
            )
    
    def get_students(self):
        mycursor = self.db.cursor()
        mycursor.execute("Select * from tbstudents")
        return mycursor.fetchall()

    def add_student(self, std):
        mycursor = self.db.cursor()
        sql = "insert into tbstudents (student_name, matric, level) VALUES (%s,%s,%s)"
        mycursor.execute(sql, std)
        self.db.commit()