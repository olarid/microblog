class Student:
    def __init__(self, name):
        self.Name = name
        self.Courses = []
    
    def addCourse(self):
        raw = input("Input Course Code, Unit, Score: \t")
        cs = raw.split(",")
        if(len(cs) < 3):
            print("Please input in format code,unit,score")
            return False
        gp = self.calcGradePoint(float(cs[2].strip()))
        course = {
            "Course": cs[0].strip(),
            "Unit": int(cs[1].strip()),
            "Score": float(cs[2].strip()),
            "Grade": gp['grade'],
            "WGP": gp['point'] * float(cs[1].strip())
        }
        self.Courses.append(course)
    
    #calculate alphabetic grade
    def calcGradePoint(self, sc):
        grade = ""
        point = 0
        if sc >= 70:
            grade = "A"
            point = 5
        elif sc >= 60:
            grade = "B"
            point = 4
        elif sc >= 50:
            grade = "C"
            point = 3
        elif sc >= 45:
            grade = "D"
            point = 2
        elif sc >=40:
            grade = "E"
            point = 1
        else:
            grade = "F"
            point = 0
        gp = {
            "grade":grade,
            "point":point
        }
        return gp


    def printGPA(self):
        tpoints = 0
        tunits = 0
        cgpa = 0.0
        print("\n\nCourse\t\tUnit\tScore\tGrade")
        for c in self.Courses:
            tpoints += c['WGP']
            tunits += c['Unit']
            print("{0}\t\t{1}\t{2}\t{3}".format(c['Course'], c['Unit'], c['Score'], c['Grade']))
        
        if(tpoints > 0):
            cgpa = tpoints/tunits
        print("\n\nCGPA is {:.2f}".format(cgpa) )

        

st = Student(input("What is your name?\t"))

while True:
    dec = input("Prease 1 to input course, -1 to stop:\t")
    if dec == '-1':
        st.printGPA()
        break
    else:
        st.addCourse()