#keeps students records in a .txt file 
import os.path
import dbwork
class Students:
    def __init__(self):
        self.filename = "students.txt"
        print("got to init")
        self.db = dbwork.dbwork()
    
    def add(self,student):
        std = student.split(',')
        if len(std) < 3:
            print("Input not in correct format")
            return
        #Save to database 
        self.db.add_student( (std[0], std[1], std[2]) )

    def print(self):
        for s in self.db.get_students():
            print(s)

std = Students()

while True:
    name = input("input student name, matric no and level, -1 to quit, p to print:\t")
    if(name == '-1'):
        std.print()
        break
    if(name == 'p'):
        std.print()
        continue
    std.add(name)